terraform {
  required_providers {
    azurerm = {
        source = "hashicorp/azurerm"
        version = "=3.48.0"

    }
  }
  backend "azurerm" {
    resource_group_name  = "peterrg"
    storage_account_name = "peterveshisa"
    container_name       = "containeruno"
    key                  = "provivders.tfstate"
  }
  
}

provider "azurerm" {
    features{}
}


data "azurerm_image" "main" {
  name = "peterUbuntuImage"
  resource_group_name = "peterrg"
}

output "id" {
  value = data.azurerm_image.main.id
}
output "image_id" {
  value       = "/subscriptions/c0177cf4-3641-4a06-bbf6-40d1ac0a60f8/resourceGroups/peterrg/providers/Microsoft.Compute/images/peterUbuntuImage"
}


