
resource "azurerm_virtual_machine" "custom-image-vm" {
    name = "custom-image-vm"
    location = azurerm_resource_group.peterrg.location
    resource_group_name = azurerm_resource_group.peterrg.name
    network_interface_ids = [azurerm_network_interface.peter-nic.id]
    vm_size = "Standard_DS1_v2"
    delete_os_disk_on_termination = true 
    delete_data_disks_on_termination = true


    storage_image_reference {
        
        id = "${data.azurerm_image.main.id}"
    }

    storage_os_disk {
        name = "peterosdisk2"
        caching = "ReadWrite"
        create_option = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    os_profile {
        computer_name = "peterimage"
        admin_username = "peter"
        admin_password = "Pjepri132!"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }
}

resource "azurerm_resource_group" "peterrg" {
    name = var.name
    location = var.location
}

resource "azurerm_storage_account" "petersa" {
  name                     = "peterveshisa"
  resource_group_name      = azurerm_resource_group.peterrg.name
  location                 = azurerm_resource_group.peterrg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
resource "azurerm_storage_container" "petersac" {
  name                  = "containeruno"
  storage_account_name  = azurerm_storage_account.petersa.name
  container_access_type = "private"
}

resource "azurerm_virtual_network" "petervn" {
  name                = "petervn"
  location            = azurerm_resource_group.peterrg.location
  resource_group_name = azurerm_resource_group.peterrg.name
  address_space       = ["10.0.0.0/16"]

}

resource "azurerm_subnet" "petersubnet" {
    name = "petervnsubnet"
    resource_group_name = azurerm_resource_group.peterrg.name
    virtual_network_name = azurerm_virtual_network.petervn.name
    address_prefixes = ["10.0.3.0/24"]
}

resource "azurerm_network_security_group" "petersg" {
  name                = "peter-seciurity-group"
  location            = azurerm_resource_group.peterrg.location
  resource_group_name = azurerm_resource_group.peterrg.name
  
  
  security_rule {
    name                       = "allow-on-port-433"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "433"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow-on-port-80"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow-ssh-office-ip"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "80.91.117.70"
    destination_address_prefix = "*"
  }

  
}

resource "azurerm_subnet_network_security_group_association" "nsg-association" {
  subnet_id = azurerm_subnet.petersubnet.id
  network_security_group_id = azurerm_network_security_group.petersg.id
  depends_on = [
    azurerm_network_security_group.petersg
  ]
}


resource "azurerm_public_ip" "peter-public-ip" {
  name                = "peter-public-ip"
  resource_group_name = azurerm_resource_group.peterrg.name
  location            = azurerm_resource_group.peterrg.location
  allocation_method   = "Dynamic"

}

resource "azurerm_network_interface" "peter-nic" {
    name = "peter-nic"
    location = azurerm_resource_group.peterrg.location
    resource_group_name = azurerm_resource_group.peterrg.name


    ip_configuration {
        name = "peter-config"
        subnet_id = azurerm_subnet.petersubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id = azurerm_public_ip.peter-public-ip.id
    }
}

